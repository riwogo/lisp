from expression import Expression
from environment import Environment


class SelfEvaluating(Expression):
    def eval(self, env: Environment) -> 'SelfEvaluating':
        return self