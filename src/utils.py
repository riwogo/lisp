from typing import List

from expression import Expression
from self_evaluating import SelfEvaluating
from environment import Environment


def eval_sequence(exps: List[Expression], env: Environment) -> SelfEvaluating:
    if (len(exps) == 1):
        return exps[0].eval(env)

    exps[0].eval(env)
    return eval_sequence(exps[1:], env)
