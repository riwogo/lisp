from typing import List, Any

from expression import Expression
from self_evaluating import SelfEvaluating
from procedure import Procedure
from environment import Environment


class Application(Expression):
    def __init__(self, operator: Procedure, operands: List[Expression]) -> None:
        self.operator = operator
        self.operands = operands

    def eval(self, env: Environment) -> SelfEvaluating:
        list_of_values = list(map(lambda op: op.eval(env), self.operands))
        return self.operator.eval(env).apply(list_of_values)