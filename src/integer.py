from self_evaluating import SelfEvaluating
from environment import Environment


class Integer(SelfEvaluating):
    def __init__(self, value: int) -> None:
        self.value = value

    def eval(self, env: Environment) -> 'Integer':
        return self

    def __add__(self, other: 'Integer') -> 'Integer':
        return Integer(self.value + other.value)

    def __mul__(self, other: 'Integer') -> 'Integer':
        return Integer(self.value * other.value)

    def __str__(self) -> str:
        return str(self.value)
