from typing import List, Any
from abc import ABC, abstractmethod

from expression import Expression
from self_evaluating import SelfEvaluating
from variable import Variable
from environment import Environment


TOO_FEW_ARGS_ERR_MSG = 'Too few arguments supplied!!!'
TOO_MANY_ARGS_ERR_MSG = 'Too many arguments supplied!!!'

class Procedure(SelfEvaluating, ABC):
    def __init__(self, params: List[Variable], body: List[Expression], env: Environment) -> None:
        self.params = params
        self.body = body
        self.env = env

    def eval(self, env: Environment) -> 'Procedure':
        return self

    def check_params_args_eq(self, args: List[SelfEvaluating]) -> None:
        if (len(self.params) != len(args)):
            if (len(self.params) < len(args)):
                raise Exception(TOO_MANY_ARGS_ERR_MSG)
            else:
                raise Exception(TOO_FEW_ARGS_ERR_MSG)

    def pre_apply(self, args: List[SelfEvaluating]) -> Environment:
        self.check_params_args_eq(args)
        new_env = self.env.add_all(self.params, args) 
        
        return new_env

    @abstractmethod
    def apply(self, args: List[SelfEvaluating]) -> SelfEvaluating:
        pass