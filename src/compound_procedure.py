from typing import List

from procedure import Procedure
from self_evaluating import SelfEvaluating
from utils import eval_sequence


class CompoundProcedure(Procedure):
    def apply(self, args: List[SelfEvaluating]) -> SelfEvaluating:
        new_env = super().pre_apply(args)
        return eval_sequence(self.body, new_env)