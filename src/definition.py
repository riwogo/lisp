from expression import Expression
from self_evaluating import SelfEvaluating
from variable import Variable
from environment import Environment


class Definition(Expression):
    def __init__(self, variable: Variable, value: Expression) -> None:
        self.variable = variable
        self.value = value

    def eval(self, env: Environment) -> SelfEvaluating:
        evaluatedValue = self.value.eval(env)
        env.add_mut(self.variable, evaluatedValue)

        return evaluatedValue