from abc import ABC, abstractmethod
from typing import List

from procedure import Procedure
from self_evaluating import SelfEvaluating


class PrimitiveProcedure(Procedure, ABC):
    def apply(self, args: List[SelfEvaluating]) -> SelfEvaluating:
        _ = super().pre_apply(args)
        
        return self.operate(args)

    @abstractmethod
    def operate(self, args: List[SelfEvaluating]) -> SelfEvaluating:
        pass