from expression import Expression
from self_evaluating import SelfEvaluating
from environment import Environment


class If(Expression):
    def __init__(self, predicate: Expression, consequent: Expression, alternative: Expression) -> None:
        self.predicate = predicate
        self.consequent = consequent
        self.alternative = alternative

    def eval(self, env: Environment) -> SelfEvaluating:
        if self.predicate.eval(env):
            return self.consequent.eval(env)
        else:
            return self.alternative.eval(env)