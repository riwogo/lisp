from expression import Expression
from self_evaluating import SelfEvaluating
from environment import Environment


class Variable(Expression):
    def __init__(self, name: str) -> None:
        self.name = name

    def eval(self, env: Environment) -> SelfEvaluating:
        return env.get(self)