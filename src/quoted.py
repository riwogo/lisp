from expression import Expression
from self_evaluating import SelfEvaluating
from environment import Environment


class Quoted(SelfEvaluating):
    def __init__(self, exp: Expression) -> None:
        self.exp = exp

    def eval(self, env: Environment) -> 'Quoted':
        return self