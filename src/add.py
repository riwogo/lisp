from typing import List, Union

from primitive_procedure import PrimitiveProcedure
from integer import Integer


Addable = Union[Integer]

class Add(PrimitiveProcedure):
    def operate(self, args: List[Addable]) -> Addable:
        return args[0] + args[1]