from typing import List, Union
from abc import ABC, abstractmethod

from environment import Environment


class Expression(ABC):
    @abstractmethod
    def eval(self, env: Environment) -> SelfEvaluating:
        pass