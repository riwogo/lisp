from typing import List, Union, Any

from expression import Expression
from self_evaluating import SelfEvaluating
from environment import Environment
from utils import eval_sequence


class Begin(Expression):
    def __init__(self, exps: List[Expression]) -> None:
        self.exps = exps

    def eval(self, env: Environment) -> SelfEvaluating:
        return eval_sequence(self.exps, env)
