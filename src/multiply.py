from typing import List, Union

from primitive_procedure import PrimitiveProcedure
from integer import Integer


Mulable = Union[Integer]

class Multiply(PrimitiveProcedure):
    def operate(self, args: List[Mulable]) -> Mulable:
        return args[0] * args[1]