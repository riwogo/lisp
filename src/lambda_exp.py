from typing import List

from expression import Expression
from variable import Variable
from compound_procedure import CompoundProcedure
from environment import Environment


class Lambda(Expression):
    def __init__(self, params: List[Variable], body: List[Expression]) -> None:
        self.params = params
        self.body = body

    def eval(self, env: Environment) -> CompoundProcedure:
        return CompoundProcedure(self.params, self.body, env)