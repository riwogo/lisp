from typing import Dict, List, Tuple

from self_evaluating import SelfEvaluating
from variable import Variable


class Environment:
    def __init__(self, dict: Dict[str, SelfEvaluating]) -> None:
        self.dict = dict

    def add(self, var: Variable, value: SelfEvaluating) -> 'Environment':
        vars_vals: List[Tuple[str, SelfEvaluating]] = list(self.dict.items()) + [(var.name, value)]
        return Environment(dict(vars_vals))

    def add_mut(self, var: Variable, value: SelfEvaluating) -> None:
        self.dict[var.name] = value

    def add_all(self, variables: List[Variable], values: List[SelfEvaluating]) -> 'Environment':
        vars_vals: List[Tuple[str, SelfEvaluating]] = list(self.dict.items()) + list(
            zip(
                map(
                    lambda var: var.name, variables),
                values))

        return Environment(dict(vars_vals))

    def get(self, var: Variable) -> SelfEvaluating:
        return self.dict[var.name]
